function credential_factory(callback) {
    function getCookie(name) {
        let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }
    nacl_factory.instantiate(function (nacl_) {
        const credentials = {ClientKeypair: nacl_.crypto_box_keypair(),
                             // ClientKeypair: nacl_.crypto_box_seed_keypair(),
                             ClientSigningKeypair: nacl_.crypto_sign_keypair(),
                             ServerPublicKey: nacl_.from_hex(getCookie("serverpubkey")),
                             ServerVerifyKey: nacl_.from_hex(getCookie("serververkey")),
                             nacl: nacl_};
        callback(credentials);
    });
}

function encrypt(message, credentials) {
    let nonce = credentials.nacl.crypto_box_random_nonce();
    let ciphertext = credentials.nacl.crypto_box(credentials.nacl.encode_utf8(message), nonce, credentials.ServerPublicKey, credentials.ClientKeypair.boxSk);

    let digest = credentials.nacl.crypto_hash(ciphertext);
    let signature = credentials.nacl.crypto_sign(digest, credentials.ClientSigningKeypair.signSk);

    return {
        "Nonce": credentials.nacl.to_hex(nonce),
        "ClientPubKey": credentials.nacl.to_hex(credentials.ClientKeypair.boxPk),
        "Ciphertext": credentials.nacl.to_hex(ciphertext),
        "ClientVerifyKey": credentials.nacl.to_hex(credentials.ClientSigningKeypair.signPk),
        "Signature": credentials.nacl.to_hex(signature)
    };
}

function decrypt(nonce, client_ciphertext, signature, credentials) {
    let decoded = credentials.nacl.crypto_box_open(
        credentials.nacl.from_hex(client_ciphertext),
        credentials.nacl.from_hex(nonce),
        credentials.ServerPublicKey,
        credentials.ClientKeypair.boxSk);

    let plaintext = credentials.nacl.decode_utf8(decoded);
    let digest = credentials.nacl.crypto_hash(credentials.nacl.from_hex(client_ciphertext));

    // TODO: !IMPORTANT! Verify the origin of the decrypted message!
    // let verify = credentials.nacl.crypto_sign_open(credentials.nacl.from_hex(signature), credentials.ServerVerifyKey);
    //
    // if (verify == null) {
    //     throw "Verification failed";
    // } else if (verify === digest) {
    //     return plaintext;
    // } else {
    //     throw "Unknown Verification Failure"
    // }
    return plaintext;
}