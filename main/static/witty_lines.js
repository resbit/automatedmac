function line() {
    let witty_lines = [
        "Something didn't go as planned.",
        "Maybe you broke something?",
        "You done did it again...",
        "Quick Ma! Call the administration!",
        "Look ma, no hands!",
        "Task failed successfully",
        "It seems like you haven't gotten an error in a while.",
        "Something bad",
        "Run away as fast as you can",
        "Catastrophic failure",
        "PEBCAK",
        "Biological Interface Error",
        "Everything is fine. Trust me.",
        "Oops",
        "This is a good time to display an error",
        "Why did you do that?",
        "I would blame the sysadmins",
        "Well this is awkward...",
        "That hurt",
        "Oof",
        "Here's some cake to make you feel better. &#x1F370;"
    ];
    let min = 0;
    let max = witty_lines.length;

    let prng = new Math.seedrandom();

    let random = Math.floor(prng() * (+max - +min)) + +min;

    document.getElementById("witty_line").innerHTML = witty_lines[random];
}