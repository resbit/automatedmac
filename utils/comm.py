from typing import Union, Optional


class ErrorCode(Exception):
    def __init__(self, code: Optional[int] = None, message: Optional[str] = None):
        self.code = code
        self.message = message

    def __str__(self):
        return repr(self.code)


class InitError(ErrorCode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class InterfaceError(ErrorCode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class RegistrationError(ErrorCode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Status:
    def __init__(self, success: bool, status_code: Optional[ErrorCode], message: Optional[str]):
        self.success = success
        self.message = message
        self.status_code = status_code
