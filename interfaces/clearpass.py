import json
import os
from datetime import datetime, date
from typing import Union
from urllib.parse import quote

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

headers = {"Content-Type": "application/json"}

class ClearpassAPI:
    """
    Interface with Aruba Clearpass for devices
    """

    def __init__(self):
        clearpass_config_path = os.path.dirname(os.path.abspath(__file__)) + "/../configs/clearpass_client_secret.json"
        with open(clearpass_config_path, 'r') as f:
            data = json.load(f)
            self.CLIENT_SECRET = data['SecretKey']
            self.CLIENT_ID = data['ClientID']
            self.API_ENDPOINT = data['APIEndPoint']
        self.auth_token = self.get_token()
        self.headers = {"Content-Type": "application/json",
                        "Authorization": "{0}".format(self.auth_token)}

    def patch_device(self, id_: str, mac: str) -> dict:
        """
        Modify MAC address of existing entry

        Parameters
        -----
        id_ : str
            Clearpass ID
        mac : str
            New mac address of device

        Returns
        -----
        dict
            User object from Aruba if successful, otherwise errors
        """

        url = self.API_ENDPOINT + "device/" + str(id_)
        payload = {"mac": mac}
        r = requests.patch(url, data=json.dumps(payload), headers=self.headers, verify=False)
        return json.loads(r.text)

    def create_device(self, email: str, mac: str, expire_time: date = None, expire_action: int = 2) -> dict:
        """
        Create a new device

        Parameters
        -----
        email : str
            Clearpass ID
        mac : str
            New mac address of device
        expire_time : date
            Default: None
            Datetime object for expire time
        expire_action : int
            Default: 2
            What to do when account expires

        Returns
        -----
        dict
            User object from Aruba if successful, otherwise errors
        """

        url = self.API_ENDPOINT + "device"
        # TODO: Fix timezone??
        if expire_time is None:
            date_ = datetime(datetime.today().year + 4, 9, 9)
            expire_time = int(date_.timestamp())
        payload = {
            "start_time": "{0}".format(int(datetime.now().timestamp())),
            "enabled": True,
            "expire_time": "{0}".format(expire_time),
            "do_expire": expire_action,  # 2=Disable and logout at specified time
            "mac": mac,
            "notes": "",
            "role_id": 2,  # Guest
            "visitor_name": "{0}".format(email)
        }
        r = requests.post(url, data=json.dumps(payload), headers=self.headers, verify=False)
        return json.loads(r.text)

    def check_device(self, email: str = None, mac: str = None) -> Union[dict, None]:
        """
        Query a device in Clearpass

        Parameters
        -----
        email : str
            Clearpass ID
        mac : str
            Mac address of device

        Notes
        -----
        Email and mac addresses are mutually exclusive arguments

        Returns
        -----
        dict
            User object from Aruba if successful, otherwise errors
        None
            If parameters were not satisfied, None type will be returned
        """

        if email is not None:
            json_filter = {"visitor_name": "{0}".format(email)}
            url = self.API_ENDPOINT + "device/?filter=" + quote(json.dumps(json_filter)) + \
                  "&sort=-id&offset=0&limit=100&calculate_count=false"
            r = requests.get(url, headers=self.headers, verify=False)
            return json.loads(r.text)
        elif mac is not None:
            url = self.API_ENDPOINT + 'device/mac/' + mac
            r = requests.get(url, headers=self.headers, verify=False)
            return json.loads(r.text)
        return None

    def delete_device(self, id_: str = None, mac: str = None) -> Union[dict, None]:
        """
        Create a new device

        Parameters
        -----
        id_ : str
            Clearpass ID
        mac : str
            Mac address of device

        Notes
        -----
        ID and mac addresses are mutually exclusive arguments

        Returns
        -----
        dict
            User object from Aruba if successful, otherwise errors
        """

        r = None
        if id_ is not None:
            url = self.API_ENDPOINT + "device/" + str(id_)
            r = requests.delete(url, headers=self.headers, verify=False)
        elif mac is not None:
            url = self.API_ENDPOINT + "device/mac/" + mac
            r = requests.delete(url, headers=self.headers, verify=False)
        if r is None:
            return None
        elif r.status_code != 200:
            return r.status_code
        return None

    def get_token(self) -> str:
        """
        Obtain access token from Clearpass

        Returns
        -----
        auth_token
            Access token in format "Bearer <token>"
        """

        url = self.API_ENDPOINT + "oauth"
        payload = {"grant_type": "client_credentials",
                   "client_id": "{0}".format(self.CLIENT_ID),
                   "client_secret": "{0}".format(self.CLIENT_SECRET),
                   "scope": "",
                   "refresh_token": ""
                   }
        r = requests.post(url, data=json.dumps(payload), headers=headers, verify=False)
        print(r.text, "\n\n\n\n")
        json_data = json.loads(r.text)
        auth_token = "Bearer " + json_data['access_token']
        return auth_token
