from django.apps import AppConfig
from config.cfg import Cfg


class MainConfig(AppConfig):
    name = 'main'

    def ready(self):
        Cfg.init()
