function enterPopup() {
	document.querySelector('#codePopupBackground').classList.add('codePopupActive');
	document.querySelector('#codePopupBackground').classList.remove('codePopupInactive');
	document.querySelector('#codePopupBackground').classList.remove('codePopupInactiveEnd');
}

function exitPopup() {
	document.querySelector('#codePopupBackground').classList.remove('codePopupActive');
	document.querySelector('#codePopupBackground').classList.add('codePopupInactive');
    setTimeout(function () {
    	document.querySelector('#codePopupBackground').classList.add('codePopupInactiveEnd');
    }, 400);
}