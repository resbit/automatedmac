import requests
import hashlib


def has_internet():
    try:
        requests.get('http://www.google.com/', timeout=5)
        return True
    except Exception as e:
        pass
    return False


def file_update(local_file_loc, remote_file_url):
    m = hashlib.md5()
    n = hashlib.md5()

    try:
        with open(local_file_loc, 'rb') as fh:
            while True:
                data = fh.read(8192)
                if not data:
                    break
                n.update(data)
    except IOError:
        pass

    response = requests.get(remote_file_url)
    response.raise_for_status()

    for data in response.iter_content(8192):
        m.update(data)

    if m.hexdigest() != n.hexdigest():
        with open(local_file_loc, 'wb') as handle:
            for block in response.iter_content(1024):
                handle.write(block)
        return True
    return False
