from datetime import datetime

import pyotp
import time

from config.cfg import Cfg
from utils import comm


def make_totp(duration, sponsor):
    if duration not in list(Cfg.totp_keys.keys()):
        raise comm.ErrorCode(message='Invalid duration')

    if sponsor not in list(Cfg.administrators.keys()):
        raise comm.ErrorCode(message='Invalid sponsor')

    sponsor_id = Cfg.administrators[sponsor][:Cfg.totp_settings['AdminIDLength']]

    return str(pyotp.TOTP(Cfg.totp_keys[duration],
                          digits=Cfg.totp_settings['TOTPLength'],
                          interval=Cfg.totp_settings['Interval']).now())\
        + sponsor_id


def time_remaining(interval: int) -> int:
    """
    Gets the time remaining for TOTP

    Parameters
    -----
    interval : int
        Time interval for TOTP

    Returns
    -----
    int
        Time remaining for TOTP period
    """

    i = time.mktime(datetime.now().timetuple())
    return int(interval - (i % interval))
